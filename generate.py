#!/usr/bin/env python3

"""

Ce programme est publié sous la licence Creative Commons CC0 1.0
consultable à cette adresse :
<https://creativecommons.org/publicdomain/zero/1.0/legalcode.fr>

"""

import sys
from datetime import date
from datetime import timedelta
import subprocess
import json
import os
import os.path
from functools import reduce
from jsonschema import validate
import locale


def get_bill_schema():
    return {
        'type': 'object',
        'properties': {
            'user.id': { 'type': 'number' },
            'user.name': { 'type': 'string' },
            'user.company.number': { 'type': 'string' },
            'user.company.address': {
                'type': 'array',
                'items': [
                    { 'type': 'string' },
                    { 'type': 'string' },
                    { 'type': 'string' }
                ],
                'additionalItems': False
            },
            'user.phoneNumber': { 'type': 'string' },
            'user.email': { 'type': 'string' },
            'number': { 'type': 'number' },
            'billingAddress': {
                'type': 'array',
                'items': [
                    { 'type': 'string' },
                    { 'type': 'string' },
                    { 'type': 'string' }
                ],
                'additionalItems': False
            },
            'lines': {
                'type': 'array',
                'items': {
                    'type': 'object',
                    'properties': {
                        'label': { 'type': 'string' },
                        'dailyTariff': { 'type': 'number' },
                        'startingDate': { 'type': 'string' },
                        'endingDate': { 'type': 'string' },
                    }
                },
                'additionalItems': False
            }
        }
    }

def date_lisible(la_date):
    return date.fromisoformat(la_date).strftime('%A %d %B %Y')

def generate_lines(bill):
    line = '\ligne{{{}, forfait journalier: {} € (du {} au {})}}[1]{{{}}}\n'

    lines = []
    for service in bill['lines']:
        nb_jours = (date.fromisoformat(service['endingDate']) - date.fromisoformat(service['startingDate'])).days
        service['label'] = service['label'] + ' ' + str(nb_jours)

        lines.append(line.format(service['label'], service['dailyTariff'],
                                 date_lisible(service['startingDate']),
                                 date_lisible(service['endingDate']),
                                 service['dailyTariff'] * nb_jours))

    return lines

def generate_latex_bill(bill):
    model = ''

    with open('model.tex', 'r') as f:
        model += f.read()

    model = model.replace('NUMFACTURE', str(bill['number']))
    model = model.replace('NOM', bill['user.name'])
    model = model.replace('NUMSIREN', bill['user.company.number'])
    model = model.replace('ADRESSEÉLECTRONIQUE', bill['user.email'])
    model = model.replace('TÉLÉPHONE', bill['user.phoneNumber'])

    if len(bill['lines']) > 0:
        model = model.replace('LIGNES', reduce(lambda x, y: x + y, generate_lines(bill)))
    else:
        model = model.replace('LIGNES', '')

    # Les adresses
    address = ''
    for élément in bill['user.company.address']:
        address += élément + '\\\\\n'
    model = model.replace('ADRESSE', address)

    entreprise = ''
    for élément in bill['billingAddress']:
        entreprise += élément + '\\\\\n'
    model = model.replace('ENTREPRISE', entreprise)

    return model

def critical_error(exception):
    print(json.dumps({ 'error': 'critical error: {}'.format(exception) }, indent=2))

def latex_compile(bill_path):
    subprocess.run(['/usr/bin/xelatex',  '\\nonstopmode\\input',
                    os.path.basename(bill_path)],
                   cwd=os.path.dirname(bill_path),
                   stdout=False, stderr=False)

    pdf_path = bill_path.split('.tex')[0] + '.pdf'
    if os.path.exists(pdf_path):
        return pdf_path
    else:
        raise RuntimeError('Can\'t compile latex bill.')

def main(arguments):
    locale.setlocale(locale.LC_ALL, '')

    # Vérifier qu’un autre programme de génération n’est pas déjà
    # lancé.
    if os.path.exists('generating.lock'):
        print(json.dumps({ 'error': 'A bill is already being generated.' }, indent=2))
        sys.exit(1)

    exit_code = 0
    try:
        with open('generating.lock', 'w') as f:
            f.write(str(os.getpid()))

        # Obtenir la facture dans son format JSON depuis l’entrée standard.
        bill = json.loads(sys.stdin.read())
        validate(instance=bill, schema=get_bill_schema())

        # Créer le dossier de l’utilisateur s’il n’existe pas.
        os.mkdir(str(bill['user.id']))

        bill_path = '{}/bill#{}.tex'.format(bill['user.id'], bill['number']);

        # Générer le fichier latex à compiler.
        with open(bill_path, 'w') as f:
            f.write(generate_latex_bill(bill))

        # Le compiler.
        pdf_path = latex_compile(bill_path)
        print(json.dumps({ 'path': os.path.abspath(pdf_path) }, indent=2))
    except Exception as e:
        critical_error(e)
        exit_code = 1
    finally:
        os.unlink('generating.lock')

    sys.exit(exit_code)

if __name__ == "__main__":
    main(sys.argv)
